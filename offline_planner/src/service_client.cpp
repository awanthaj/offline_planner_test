//////////////////////////////////////////////////////////////////////////////////////////////////////////
//   Project      : OFFLINE PATH PLANNER FOR BELL 412 HELICOPTOR                                        //
//   File         : Service_client.cpp                                                                  //  
//   Description  : ROS service client for invoking service server to calculate the offline             //
//                  planning solution and generate reference points necessary for trajectory control.   //     
//   Created On   : 28/02/2021                                                                          //
//   Created By   : Awantha Jayasiri <mailto:awantha.jayasiri@nrc-cnrc.gc.ca>                           //
//////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <nlohmann/json.hpp>
#include "ros/ros.h"
#include "std_srvs/Empty.h"
#include <nav_msgs/Path.h>
//#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseStamped.h>
//#include <tf/transform_listener.h>
#include "offline_planner/Mission_Service.h"
#include <offline_planner/display.hpp>  //For printing the data from service end for testing
#include <cstdlib>
#include <visualization_msgs/Marker.h>
#include<visualization_msgs/MarkerArray.h>
#include <cmath>
#include <string>
#include <std_msgs/Float64.h>
#include <offline_planner/my_structs.hpp>
#include <offline_planner/my_functions.hpp>

////////////// For MAVROS
#include <mavros_msgs/WaypointPush.h>
#include <mavros_msgs/WaypointClear.h>
#include <mavros_msgs/CommandHome.h>
#include <mavros_msgs/Waypoint.h>

////////////////////

std::vector <geometry_msgs::Point> corridor(const geometry_msgs::Point& pt1,const geometry_msgs::Point& pt2, const double& lm, const double& rm)
{ std::vector <geometry_msgs::Point> tunnel_vec;
geometry_msgs::Point p1; geometry_msgs::Point p2; geometry_msgs::Point p3; geometry_msgs::Point p4;    
 p1.x=pt1.x; p1.y=pt1.y; p1.z=pt1.z; p2.x=pt2.x; p2.y=pt2.y; p2.z=pt2.z; p3=p2, p4=p1;   
  double dx=pt2.x-pt1.x;  double dy=pt2.y-pt1.y;
  double normv=sqrt((dx*dx) + (dy*dy)) ; 
  double left_vect_x=-(dy/normv)*lm; double left_vect_y=(dx/normv)*lm;
  double right_vect_x=(dy/normv)*rm; double right_vect_y=-(dx/normv)*rm;
  p1.x+=left_vect_x; p1.y+=left_vect_y; p2.x+=left_vect_x; p2.y+=left_vect_y;
  p3.x+=right_vect_x; p3.y+=right_vect_y; p4.x+=right_vect_x; p4.y+=right_vect_y;
  tunnel_vec.push_back(p1); tunnel_vec.push_back(p2); tunnel_vec.push_back(p3); tunnel_vec.push_back(p4);
  return tunnel_vec;
}

int main(int argc, char **argv)
{
///////////////////////////////////////////////////////////////
//std::ifstream ifs("/home/awantha/catkin_ws2/src/offline_planner/plan_files/Ottawa_Mission.plan");
std::ifstream ifs("/home/awantha/catkin_ws2/src/offline_planner/plan_files/MSNOL6-2.plan");
//std::ifstream ifs("/home/awantha/catkin_ws2/src/offline_planner/plan_files/pgncc2-EY_CCT.plan");

nlohmann::json plan_file=nlohmann::json::parse(ifs);

mission_struct current_mission=parse_plan_file(plan_file);

//std::vector <point_LatLonAltVel>PLAN_WPs_Old= Generate_plan_WPs_LatLonAlt(current_mission);
std::vector <point_xyzvdldrfo>XYZ_wps_old= Generate_plan_WPs_XYZ(Generate_plan_WPs_LatLonAlt(current_mission));

std::vector <geometry_msgs::Point> XYZ_wps;
for (int i=0; i < XYZ_wps_old.size();i++){
  geometry_msgs::Point p1; p1.x=XYZ_wps_old.at(i).x; p1.y=XYZ_wps_old.at(i).y; p1.z=XYZ_wps_old.at(i).z;
  XYZ_wps.push_back(p1);
 std::cout<<"LatLonAltVel:  "<<XYZ_wps_old.at(i).x<<", "<<XYZ_wps_old.at(i).y<<", "<<XYZ_wps_old.at(i).z<<", "<<XYZ_wps_old.at(i).v<< std::endl;
}

mission_struct new_mission= process_mission_items(current_mission);
nlohmann::json new_plan_file;  
// Make a json object using new_mission struct.
nlohmann::json newj=new_mission;
for (auto it =plan_file.begin(); it != plan_file.end(); ++it)
{
   if(it.key() == "mission") new_plan_file[it.key()]=newj;
   else  new_plan_file[it.key()]=it.value();  
}
// Generating the new plan (json) file.
 std::ofstream file("/home/awantha/catkin_ws2/src/offline_planner/plan_files/test.json"); 
 file << std::setw(4)<<new_plan_file<<std::endl;

///////////////////////////////////////////////////////////


  // Initialize the ROS client Node
  ros::init(argc, argv, "mission_service_client_node");  // client node name
  ros::NodeHandle nh;
  //ros::NodeHandle nh2;

  ros::Publisher pub = nh.advertise<nav_msgs::Path>("/path",1000);
  ros::Publisher pub_h = nh.advertise< std_msgs::Float64>("/heading",1000);
  ros::Publisher pub_b = nh.advertise< std_msgs::Float64>("/bank",1000);
  ros::Publisher pub_v = nh.advertise< std_msgs::Float64>("/velocity",1000);
  ros::Publisher marker_pub = nh.advertise<visualization_msgs::Marker>( "/visualization_marker", 10 );
  ros::Publisher markerArray_pub = nh.advertise<visualization_msgs::MarkerArray>("/MarkerArray", 10);
  ros::Rate loop_rate(10);

  ros::ServiceClient mission_client = nh.serviceClient<offline_planner::Mission_Service>("mission_service"); // create the client (mission_client)
  offline_planner::Mission_Service srv;
  std::vector<double> wp_vector; // way point vector
// Adding waypoints of the mission (x,y,z,speed,x margin,y margin, is_fly_over?)
  wp_vector.push_back(-5000); wp_vector.push_back(-5000); wp_vector.push_back(0); wp_vector.push_back(0); wp_vector.push_back(300.0); wp_vector.push_back(300.0); wp_vector.push_back(0.0); // wp 0
 wp_vector.push_back(0); wp_vector.push_back(-5000); wp_vector.push_back(0); wp_vector.push_back(30); wp_vector.push_back(300.0); wp_vector.push_back(300.0); wp_vector.push_back(0.0); // wp 1
 
  wp_vector.push_back(5000); wp_vector.push_back(-5000); wp_vector.push_back(0); wp_vector.push_back(30); wp_vector.push_back(300.0); wp_vector.push_back(300.0); wp_vector.push_back(1.0);// wp 2
  wp_vector.push_back(5000); wp_vector.push_back(5000); wp_vector.push_back(0); wp_vector.push_back(30); wp_vector.push_back(300.0); wp_vector.push_back(300.0); wp_vector.push_back(0.0);// wp 3
  wp_vector.push_back(-5000); wp_vector.push_back(5000); wp_vector.push_back(0); wp_vector.push_back(20); wp_vector.push_back(300.0); wp_vector.push_back(300.0); wp_vector.push_back(0.0);// wp 4
  wp_vector.push_back(-5000); wp_vector.push_back(-5000); wp_vector.push_back(0); wp_vector.push_back(0); wp_vector.push_back(300.0); wp_vector.push_back(300.0); wp_vector.push_back(0.0);// wp 5

  srv.request.wp_number=6;//5; // number of way points
  srv.request.wp_length= 7; // data length for each waypoint

  srv.request.wp_data = wp_vector; // all waypoints are stacked one after another
      

  if (mission_client.call(srv))   // Calls the service "mission_service"
  {
    ROS_INFO("Mission service server is called and successful route is achieved!!!..");

    nav_msgs::Path path;
    path.header.frame_id="/map";
    //std::vector<geometry_msgs::PoseStamped> plan;
    for(int i=0; i<srv.response.path.path_xyz.size();i++){
     geometry_msgs:: PoseStamped pose;
     pose.header.seq=i;
     pose.header.frame_id="/map";
      pose.pose.position.x=srv.response.path.path_xyz[i].x;
      //pose.pose.position.x=XYZ_wps_old.at(i).x;
      pose.pose.position.y=srv.response.path.path_xyz[i].y;
      //pose.pose.position.y=XYZ_wps_old.at(i).y;
      pose.pose.position.z=srv.response.path.path_xyz[i].z;
      //pose.pose.position.z=XYZ_wps_old.at(i).z;
      path.poses.push_back(pose);        
     
    }
    
    visualization_msgs::MarkerArray markerArray;
  for(int f=0;f<XYZ_wps.size();f++){
  visualization_msgs::Marker  text; text.header.frame_id = "/map"; text.action= visualization_msgs::Marker::ADD; text.id = f;
   text.type= visualization_msgs::Marker::TEXT_VIEW_FACING; text.scale.x = 40; text.scale.y = 40; text.scale.z = 40;
   text.color.r = 10.0; text.color.a = 10.0;
   std::string s1=std::to_string(XYZ_wps_old.at(f).x); std::string s2=std::to_string(XYZ_wps_old.at(f).y); std::string s3=std::to_string(XYZ_wps_old.at(f).z); 
   std::string s4=std::to_string(XYZ_wps_old.at(f).v);
   std::string sx="\n"; std::string txt=s1+sx+ s2+sx+s3+sx+s4;
  text.text=txt;
  text.pose.position.x = XYZ_wps_old.at(f).x;
  text.pose.position.y = XYZ_wps_old.at(f).y;
  text.pose.position.z = XYZ_wps_old.at(f).z;
markerArray.markers.push_back(text);
}

    int cnt=0;
    while (ros::ok()){
        visualization_msgs::Marker points, line_strip, line_list, text;
        
        points.header.frame_id = line_strip.header.frame_id = line_list.header.frame_id = text.header.frame_id = "/map";
        points.header.stamp = line_strip.header.stamp = line_list.header.stamp = ros::Time::now();
        points.ns = line_strip.ns = line_list.ns = text.ns = "points_and_lines";
        points.action = line_strip.action = line_list.action =text.action= visualization_msgs::Marker::ADD;
        points.pose.orientation.w = line_strip.pose.orientation.w = line_list.pose.orientation.w = text.pose.orientation.w=1.0;
        points.id = 0;line_strip.id = 1; line_list.id = 2; text.id = 3;

        points.type = visualization_msgs::Marker::POINTS;
        line_strip.type = visualization_msgs::Marker::LINE_STRIP;
        line_list.type = visualization_msgs::Marker::LINE_LIST;
        text.type= visualization_msgs::Marker::TEXT_VIEW_FACING;

        // POINTS markers use x and y scale for width/height respectively
        points.scale.x = 50; points.scale.y = 50;
        text.scale.x = 200; text.scale.y = 200; text.scale.z = 200;
        // LINE_STRIP/LINE_LIST markers use only the x component of scale, for the line width
        line_strip.scale.x = 5; line_list.scale.x = 5;

        // Way Points are blue
        points.color.b = 1.0; points.color.a = 1.0;

        // Line list is red
        line_list.color.r = 10.0; line_list.color.a = 10.0;
        text.color.r = 10.0; text.color.a = 10.0;

        // Line strip is green
        line_strip.color.g = 1.0;line_strip.color.a = 1.0;    

///////////////////////////////////////////// FOR DISPLAY
std::vector <geometry_msgs::Point> way_points;
std::vector <geometry_msgs::Point> corridor_points;
for (int i=0; i< srv.request.wp_number-1; i++) {
geometry_msgs::Point p1; geometry_msgs::Point p2; double lm=0.0; double rm=0.0;

   p1.x =srv.request.wp_data[i*(srv.request.wp_length)];
   p1.y =srv.request.wp_data[i*(srv.request.wp_length)+1]; 
   p1.z =srv.request.wp_data[i*(srv.request.wp_length)+2]; 
   lm= srv.request.wp_data[i*(srv.request.wp_length)+4];
   rm= srv.request.wp_data[i*(srv.request.wp_length)+5];

   p2.x =srv.request.wp_data[(i+1)*(srv.request.wp_length)];
   p2.y =srv.request.wp_data[(i+1)*(srv.request.wp_length)+1]; 
   p2.z =srv.request.wp_data[(i+1)*(srv.request.wp_length)+2]; 

std::vector <geometry_msgs::Point> cps= corridor( p1, p2, lm, rm);

corridor_points.insert(std::end(corridor_points), std::begin(cps), std::end(cps));
way_points.push_back(p1);
if (i==srv.request.wp_number-2) way_points.push_back(p2);
}

std_msgs::Float64 h_msg; h_msg.data=srv.response.path.path_heading.at(cnt);
std_msgs::Float64 b_msg; b_msg.data=srv.response.path.path_bank.at(cnt);
std_msgs::Float64 v_msg; v_msg.data=srv.response.path.path_velocity.at(cnt);

//std::cout<<"cnt "<<cnt<<" "<<"vel "<<srv.response.path.path_velocity.at(cnt)<<std::endl;
//std::cout<<"cnt "<<cnt<<" "<<"x,y "<< srv.response.path.path_xyz[cnt].x<<", "<< srv.response.path.path_xyz[cnt].y<<std::endl;
if (cnt == srv.response.path.path_heading.size()-1) break;
cnt++;
//points.points=way_points;
points.points=XYZ_wps;

line_list.points=corridor_points;
line_strip.points=XYZ_wps;
marker_pub.publish(points);
// PUBLISH FOLLOWING TO GET THE PATH
marker_pub.publish(line_strip);


//if (cnt<10) 
//marker_pub.publish(text);
markerArray_pub.publish(markerArray);
//marker_pub.publish(line_list);
//pub.publish(path);
pub_h.publish(h_msg); pub_b.publish(b_msg); pub_v.publish(v_msg);
ros::spinOnce();
loop_rate.sleep();


    }
  }
  else
  {
    ROS_ERROR("Mission service server is called but route is unsuccessful. Please redefine the parameters ");
    return 1;
  }

  return 0;
}
