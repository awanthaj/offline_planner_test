//////////////////////////////////////////////////////////////////////////////////////////////////////////
//   Project      : OFFLINE PATH PLANNER FOR BELL 412 HELICOPTOR                                        //
//   File         : my_consts.hpp                                                                       //  
//   Description  : Constant declarations used by the functions in my_fuctions.cpp.                     //     
//   Created On   : 28/02/2021                                                                          //
//   Created By   : Awantha Jayasiri <mailto:awantha.jayasiri@nrc-cnrc.gc.ca>                           //
//////////////////////////////////////////////////////////////////////////////////////////////////////////


#ifndef MY_CONSTS_HPP
#define MY_CONSTS_HPP

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif


// FOR CONVERTING LAT, LON DIFFERENCES TO DISTENCES
const double EARTH_RADIUS = 6372797.56085;

// SOME CONSTANTS ARE READ THROUGH THE CURVES STRUCT FILLED BY THE CURVE_POLY_TXT

const double EPS= 1e-9 ;
const double WIND_SPEED= 0.0;
const double DIST_TOL= 1e-4;
//const double DIST_TOL_FOR_STRAIGHT_LINE= 0.001; // FOR DIFFERENCE IN STAIGHT LINE END TO CURVE START

const double GRAVITY= 9.81;
const double max_airspeed=50.0; // according to the curve coefficients file...starts with an enven number ()
const double min_airspeed=20.0;
const double max_roll=((15.0/180.0)* M_PI); // converting 15 degrees to radians
const double max_roll_rate=((15.0/180.0)* M_PI); 
const double max_roll_rate_rate=((15.0/180.0)* M_PI); 
const double max_accel=0.1*GRAVITY;
const double max_jerk=0.1*GRAVITY;
const double ASSUMED_ACCEL = 0.4 *max_accel;
const double max_vel_z=1000*0.00508; // same as defined in KiTe algorithm

const int CURV_POLY_DEG =4;
const int CURV_COEFF_LENGTH= CURV_POLY_DEG+1;
const int PSI_COEFF_LENGTH= CURV_COEFF_LENGTH+1;
const int NUM_CURVS_FOR_TURN= 3;
const int NUM_BREAKS_FOR_TURN= NUM_CURVS_FOR_TURN+1;
const int TURN_VEL_COEFF_LENGTH=2; // for now keep constant speed for entire turn for the simplecity and accelration is zero

const int SPEED_POLY_DEG =4;
const int ACCEL_POLY_DEG= 3 ;
const int NUM_SAMPLES =100;
const double TIME_WEIGHT =1.0 ;
const double CURV_SMOOTH_WEIGHT =100.0;
const int NUM_SEGMENT_SAMPLES= 25;
const double TIME_STEP =1.0;//0.1

#endif
